﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class teleport : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "teleport1")
        {
            gameObject.transform.position = new Vector3(15f, .5f, -10.1f);
        }

        if (collision.gameObject.name == "teleport2")
        {
            gameObject.transform.position = new Vector3(-8f, .5f, 3.8f);
        }
    }
}
