﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class big : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) //when the space bar is pressed, character grows
        {
           

            transform.localScale = new Vector3(2, 2, 2);

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (gameObject.transform.localScale.y > 1)
        {
            if (collision.gameObject.name == "Zombunny" || collision.gameObject.name == "Zombear"|| collision.gameObject.name == "Hellephant")
            {
                Destroy(collision.gameObject);
                transform.localScale = new Vector3(1, 1, 1); 
            }
        }
    }
}
